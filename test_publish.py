
import json
from xpms_common.mq_endpoint import MQMessage

msg_dict = """
{"header": {"from": "platform_manager", "to": "system.export_request", "msg_type": "system", "msg_id": "2586538d-14ea-4591-a63b-670997ed2686", "timestamp": "2019-05-30T09:45:40.105883"}, "payload": {"primary_key": {}, "request_id": "d74d6d03-59fc-4410-893a-2860595817cf", "trigger": "export_request", "context": {"project_name": "platform_manager", "service_name": "export_solution"}, "state": {}, "solution_id": "enso226_3d9e5715-5fcd-4512-9a45-a78314e94d3d", "entity_id": "", "data": {"request_key": "664fcb13-62b1-4d53-80ae-0806f6c45089"}, "metadata": {"response_key": "cd3de87d-68cd-45be-9926-11eef0ef71af"}, "status": {"code": 200, "traceback": "", "message": "", "error_details": {}, "success": true}, "entity_name": ""}}
"""
msg_dict = json.loads(msg_dict)
msg_dict['payload'] = MQMessage.load_payload(msg_dict['payload'])
message = MQMessage(msg_dict)
message.solution_id = "enso226_3d9e5715-5fcd-4512-9a45-a78314e94d3d"
message.metadata = dict(model=dict(model_group_name="document_classifier"))
message.trigger = "res"
message.message_type = "api"
print(message)
message.publish(routing_key="api.{}".format(message.trigger))



