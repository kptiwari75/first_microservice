import requests
from xpms_common.mq_endpoint import MQMessage


class ExternalApiTrigger(object):

    def req_res_data(msg_dict, **kwargs):
        # api-endpoint
        url = "https://reqres.in/api/users"
        params = {}
        r = requests.get(url=url, params=params)
        data = r.json()
        message = MQMessage(msg_dict)
        message.solution_id = msg_dict["payload"]["solution_id"]
        message.data = dict()
        message.trigger = "reqres"
        message.message_type = "api"
        message.metadata = data
        message.publish(routing_key="api.response")
        return
        # return data


