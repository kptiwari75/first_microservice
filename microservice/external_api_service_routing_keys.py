API_STR, SERVICE_STR, SYSTEM_STR, GEN_INS_STR = "api.", "service.", "system.", "service.generated_insight."

API_R_KEYS = ["reqres","res"]
SERVICE_R_KEYS = []
GEN_INS_R_KEYS = []
SYSTEM_R_KEYS = []


def generate_routing_keys():
    api_r_keys = [str(API_STR + key) for key in API_R_KEYS]
    service_r_keys = [str(SERVICE_STR + key) for key in SERVICE_R_KEYS]
    gen_ins_keys = [str(GEN_INS_STR + key) for key in GEN_INS_R_KEYS]
    sys_r_keys = [str(SYSTEM_STR + key) for key in SYSTEM_R_KEYS]
    return api_r_keys + service_r_keys + gen_ins_keys + sys_r_keys
