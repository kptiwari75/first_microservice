from xpms_common.mq_endpoint import MQMessage
import json
from microservice.triggers.reqres import ExternalApiTrigger
from xpms_common.xpms_logger import XPMSLogger

logger = XPMSLogger.get_instance()


class MessageHandler:

    @staticmethod
    def handle_message(message_json):
        MessageHandler.get_trigger_handler(message_json)

    @staticmethod
    def get_trigger_handler(msg_dict):
        msg_dict = json.loads(msg_dict)
        msg_dict['payload'] = MQMessage.load_payload(msg_dict['payload'])
        payload = msg_dict['payload']
        trigger = payload['trigger']
        trigger_handler = None
        try:
            if trigger == "res":
                trigger_handler = ExternalApiTrigger.req_res_data(msg_dict,)

        except Exception as e:
            print("Invalid usage error. Check your request." + str(e))



