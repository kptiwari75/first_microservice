from pika.exceptions import AMQPConnectionError, ConnectionClosed
from xpms_common.mq_consumer import MQConsumer
from microservice.external_api_service_routing_keys import generate_routing_keys
from xpms_common.errors import RabbitMQError
from xpms_common.xpms_logger import XPMSLogger, XPMSContext
from microservice.message_handler import MessageHandler
import traceback

logger = XPMSLogger.get_instance()

def process_message(body):
    MessageHandler().handle_message(body.decode('utf8'))


if __name__ == '__main__':
    lis_queue = "reqres.data"
    r_keys = generate_routing_keys()
    external_api_service_consumer = None
    try:
        # document consumer init
        external_api_service_consumer = MQConsumer(queue=lis_queue, routing_keys=r_keys, message_handler=process_message)
        external_api_service_consumer.run()
    except KeyboardInterrupt:
        if external_api_service_consumer:
            external_api_service_consumer.stop()

    except (ConnectionClosed, AMQPConnectionError)as e:
        err_obj = RabbitMQError("Connection failed " + str(e), traceback=traceback.format_exc())
        logger.log(xpms_context=XPMSContext(**{"solution_id": "system"}),
                   message=err_obj.error_message)
        exit(1)

    except Exception as e:
        err_obj = RabbitMQError("unknown error " + str(e), traceback=traceback.format_exc())
        logger.log(xpms_context=XPMSContext(**{"solution_id": "system"}),
                   message=err_obj.error_message)
        exit(1)
